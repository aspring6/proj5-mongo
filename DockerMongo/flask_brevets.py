"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', 200, type=float)
    begin_date = request.args.get('begin_date', '', type=str)#FIXME default
    begin_time = request.args.get('begin_time', '', type=str)#FIXME default
    formatted_date = begin_date + ' ' + begin_time
    fmt = "YYYY-MM-DD HH:mm"
    start = arrow.get(formatted_date, fmt).isoformat()
    #start = start.shift(hours=+1.5)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, distance, start)
    close_time = acp_times.close_time(km, distance, start)
    #start.format(fmt) -> string
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############
#
# MongoDB
#
#############

@app.route('/_show_times', methods=['GET'])
def times():
    _items = db.tododb.find()
    dates = [item for item in _items]
    for document in dates:
        document['_id'] = str(document['_id'])
    print(dates)
    return flask.jsonify(dates=dates)

@app.route('/_insert_times', methods=['POST'])
def new():
    db.tododb.drop()
    open = request.form['open']
    close = request.form['close']
    open = open.split(",")
    close = close.split(",")
    #Remove first comma separated value (empty).
    #Doing this on the backend through string slicing is faster than an if else/flag
    #in the front end
    open = open[1:]
    close = close[1:]

    print(open)
    for i in range(len(open)):
        item_doc = {
            'open': open[i],
            'close': close[i]
        }
        print(item_doc)
        db.tododb.insert_one(item_doc)

    return "hi"

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
