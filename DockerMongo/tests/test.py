"""
Nose tests for acp_times.py

"""

from acp_times import close_time, open_time

import arrow
import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

def check_times(time_one, time_two):
    assert time_one == time_two

def check_four_times(time_one, time_two, time_three, time_four):
    assert time_one == time_two and time_three == time_four


def test_zero_km_control():
    print("----- Testing test_zero_km_control() -----")
    print("Tests multiple controls at a distance of 0, with various timestamps and brevet distances.")
    print("\nEXPECTED: Open time should == the time stamp. Close times should be +1hr later")
    for i in range(0, 10):
        time_stamp = arrow.now().shift(days=+i, hours=+i)
        open_timestamp = time_stamp.isoformat()
        close_timestamp = time_stamp.shift(hours=+1).isoformat()
        yield check_four_times, open_time(0, 200, open_timestamp), open_timestamp, close_time(0,200, open_timestamp), close_timestamp

def test_control_longer_than_brevet():
    print("----- Testing test_control_longer_than_brevet() -----")
    print("Tests controls 10%% over the brevet distance.")
    print("\nEXPECTED: Open and close times should be equal to the open and close times of the brevet (the additional 10%% should make no difference)")
    brevets = [200, 300, 400, 600, 1000, 1300]
    for bv in brevets:
        time_stamp = arrow.now()
        open_timestamp = time_stamp.isoformat()
        close_timestamp = time_stamp.shift(hours=+1).isoformat()
        distance = bv * 1.1
        #NOTICE BELOW: open_time/close_time is being compared with 'bv' distance, and a distance 1.1 times greater than that
        yield check_four_times, open_time(distance, bv, open_timestamp), open_time(bv, bv, open_timestamp), close_time(distance, bv, open_timestamp), close_time(bv, bv, open_timestamp)

def test_relaxed_close_within_60km():
    print("----- Testing test_relaxed_close_within_60km() -----")
    print("Tests controls between 0 and 60km. These controls should have close times that are 'relaxed'")
    print("\nEXPECTED: Close times should behave as if they had a minimum speed of 20km/h + 1hr of the start time.")
    #Data from online calculator @ https://rusa.org/octime_acp.html
    time_stamp = arrow.now()
    start_timestamp = time_stamp.isoformat()
    assert close_time(30, 200, start_timestamp) ==  time_stamp.shift(hours=+2, minutes=+30).isoformat()

def test_control_at_180km():
    print("----- Testing test_control_at_180km() -----")
    print("Compares a control at 180km with the output from the calculator @ https://rusa.org/octime_acp.html")
    #Data from online calculator @ https://rusa.org/octime_acp.html
    time_stamp = arrow.now()
    start_timestamp = time_stamp.isoformat()
    yield check_four_times, close_time(180, 200, start_timestamp), time_stamp.shift(hours=+12).isoformat(), open_time(180, 200, start_timestamp), time_stamp.shift(hours=+5, minutes=+18).isoformat()

def test_control_at_975km():
    print("----- Testing test_control_at_975km() -----")
    print("Compares a control at 975km with the output from the calculator @ https://rusa.org/octime_acp.html")
    #Data from online calculator @ https://rusa.org/octime_acp.html
    time_stamp = arrow.now()
    start_timestamp = time_stamp.isoformat()
    yield check_four_times, close_time(975, 1000, start_timestamp), time_stamp.shift(hours=+72, minutes=+49).isoformat(), open_time(975, 1000, start_timestamp), time_stamp.shift(hours=+32, minutes=+12).isoformat()
